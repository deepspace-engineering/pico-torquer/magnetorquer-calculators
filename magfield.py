import numpy as np

# Define constants
mu_0 = 4e-7 * np.pi
G = 6.6743e-11
M_earth = 5.972e24
R_earth = 6.371e6
mu = G * M_earth

def set_spacecraft_state_from_orbital_elements(a, e, i, aop, raan, true_anomaly, mu):
    # Convert orbital elements to Keplerian elements
    p = a * (1 - e**2)
    a = np.sqrt(mu * p)

    e_vec = np.array([e * np.cos(aop), e * np.sin(aop), 0])
    i_vec = np.array([np.cos(i) * np.cos(raan), np.cos(i) * np.sin(raan), np.sin(i)])
    N_vec = np.cross([0, 0, 1], i_vec)

    # Calculate position and velocity vectors
    r_peri = p / (1 + e) * np.array([np.cos(true_anomaly), np.sin(true_anomaly), 0])
    v_peri = np.sqrt(mu / p) * np.array([-np.sin(true_anomaly), np.cos(true_anomaly), 0])
    r_sc = r_peri + a * e_vec
    v_sc = v_peri + np.cross(N_vec, r_sc) / np.linalg.norm(r_sc)

    return r_sc, v_sc

def mean_anomaly(period, t):
    mean_anomaly = (2 * np.pi * t) / period

    return mean_anomaly

def eccentric_anomaly(mean_anomaly, e, tol = 1e-6):
    E = mean_anomaly
    E_prev = 0
    
    while (abs(E - E_prev) > tol):
        E_prev = E
        E = mean_anomaly + e * np.sin(E_prev)
    
    return E

def true_anomaly_from_time(T, t, e):
    M = mean_anomaly(T, t)
    E = eccentric_anomaly(M, e, tol = 1e-6)
    true_anomaly = 2 * np.arctan(np.sqrt((1 + e) / (1 - e)) * np.tan(E / 2))
    
    if (true_anomaly < 0):
        true_anomaly = 2 * np.pi - np.abs(true_anomaly)
    
    return true_anomaly

def update_state_from_true_anomaly(nu, a, e, i, aop, raan):
    e_vec = np.array([e * np.cos(aop), e * np.sin(aop), 0])
    i_vec = np.array([np.cos(i) * np.cos(raan), np.cos(i) * np.sin(raan), np.sin(i)])
    N_vec = np.cross([0, 0, 1], i_vec)
    
    # Calculate perigee vector
    p = a * (1 - np.linalg.norm(e_vec)**2)
    r_peri = p / (1 + np.linalg.norm(e_vec)) * np.array([np.cos(nu), np.sin(nu), 0])

    # Define rotation matrices for i and ascending node
    R_i = np.array([[1, 0, 0], [0, np.cos(np.arccos(i_vec[2])), -np.sin(np.arccos(i_vec[2]))], [0, np.sin(np.arccos(i_vec[2])), np.cos(np.arccos(i_vec[2]))]])
    R_asc = np.array([[np.cos(np.arccos(N_vec[2])), -N_vec[1], -N_vec[0]], [np.sin(np.arccos(N_vec[2])), N_vec[0], -N_vec[1]], [0, 0, 1]])

    # Rotate the perigee vector
    r_sc = R_asc @ R_i @ r_peri

    # Calculate velocity vector
    f = np.sqrt(mu_0 / p)
    v_sc = np.cross(f * np.array([-np.sin(nu), np.cos(nu), 0]), r_sc)

    return r_sc, v_sc

def kepler_equations(y):
    r_sc, v_sc = y[:3], y[3:]
    r_mag = np.linalg.norm(r_sc)

    # Calculate acceleration due to gravity
    a_grav = -mu_0 * r_sc / r_mag**3

    # Form the system of differential equations
    dydt = np.concatenate((v_sc, a_grav))

    return dydt

def get_planetary_magnetic_field(mu_0, G, M_p, r):
    r_mag = np.linalg.norm(r)
    B_p = (mu_0 * G * M_p) / (4 * np.pi * r_mag**3) * r
    
    return B_p

def get_spacecraft_motional_magnetic_field(mu_0, v_sc, r):
    r_mag = np.linalg.norm(r)
    B_sc = mu_0 * np.cross(v_sc, r) / (4 * np.pi * r_mag**3)
    
    return B_sc

def get_total_magnetic_field(B_p, B_sc):
    B_tot = B_p + B_sc
    
    return B_tot