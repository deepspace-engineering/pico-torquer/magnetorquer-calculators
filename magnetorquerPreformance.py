# Calculating the performance of a PCB coil using a trapezoidal cross-section

import math

############################### USER DEFINITIONS ###############################

# Copper Laminate Material Parameters
density_Cu = 8.94 #g/cm3
resistivity_Cu = 1.724 * math.pow(10, (-8)) #ohm*meter
tempCoeff_Cu = 0.00429 # 1/C
weight_Cu_laminate = 0.5 #ounces

copper_thickness = (weight_Cu_laminate * 28.34952) / ((12 * 25.4) * (12 * 25.4) * (density_Cu / 1000)) #millimeters

# Coil Parameters
inner_diameter = 26.4 #millimeters
outer_diameter = 44 #millimeters
trace_width = 0.1524 #millimeters
trace_thickness = copper_thickness #millimeters
#trace_thickness = 0.018 #millimeters
clearance = 0.1524 #millimeters
series_or_Parallel = 1 #1: Series; 2: Parallel

# Environmental Parameters
ambient_temperature = 20 #Celcius
acceptable_temperature_rise = 10 #Celcius
Bdot = 27330.6 * math.pow(10, (-9)) #nT @ 275km above equatorial prime meridian (SSO orbit) (+/- 145nT)
desired_angular_velocity = 2 * math.pi #radians per second

# Circuitboard and Manufacturing Parameters
voltage = 1.8
permittivity = 3.72 #FR408HR @ 100MHz (dk)
loss_tangent = 0.0072 #FR408HR @ 100MHz (df)
trace_width_definition = 1 #Method 1: Trace width is etch-mask width; Method 2: Trace width is the etched base width; Method 3: Trace width is the average etched width; Method 4: Trace cross-section is rectangular
etching_angle = 45 #degrees
layers = 4

################################# CALCULATIONS #################################

# Trace Length Calculations
spiral_thickness = trace_width + clearance #thickness of the Archimedean spiral
number_of_turns_per_layer = (outer_diameter - inner_diameter) / (2 * spiral_thickness)

# Calculate trace length or spiral assuming the spiral changes radius spiral_segments_per_turn per turn
trace_length_per_layer_rounded = 0
spiral_segments_per_turn = 4

loop_steps = number_of_turns_per_layer * spiral_segments_per_turn
rounded_loop_steps = int(math.floor(loop_steps))


inner_radius = inner_diameter / 2 #mm

for i in range(0, rounded_loop_steps, 1):
    trace_length_per_layer_rounded = (inner_radius + (spiral_thickness * (i / spiral_segments_per_turn))) * ((2 * (math.pi)) / spiral_segments_per_turn) + trace_length_per_layer_rounded #millimeters

number_of_spiral_turns_fraction = loop_steps - rounded_loop_steps
trace_length_per_layer = trace_length_per_layer_rounded + (inner_radius + (spiral_thickness * ((i + 1) / spiral_segments_per_turn))) * ((2 * math.pi) * number_of_spiral_turns_fraction) #millimeters

# Trace Cross-sectional Area Calculations
match trace_width_definition:
    case 1:
        trace_crossectional_area = (trace_width * trace_thickness) + (math.pow(trace_thickness, 2) / math.tan(etching_angle * (math.pi / 180))) #mm2

    case 2:
        trace_crossectional_area = (trace_width * trace_thickness) - (math.pow(trace_thickness, 2) / math.tan(etching_angle * (math.pi / 180))) #mm2
    case 3:
        trace_crossctional_area = (trace_width * trace_thickness) + (math.pow(trace_thickness, 2) / (3 * math.tan(etching_angle * (math.pi / 180)))) #mm2
    case 4:
        trace_crossectional_area =  trace_thickness * trace_width #mm2

# Trace Resistance Calculations
resistivity_Cu_mm = resistivity_Cu * 1000 #ohm*millimeters

match series_or_Parallel:
    case 1:
        coil_length = trace_length_per_layer * layers #millimeters
        coil_resistance = resistivity_Cu_mm * (coil_length / trace_crossectional_area) * (1 + tempCoeff_Cu * (ambient_temperature + acceptable_temperature_rise)) #ohms
        total_current = voltage / coil_resistance #Amps

    case 2:
        coil_length = trace_length_per_layer #millimeters
        coil_resistance = resistivity_Cu_mm * (coil_length / trace_crossectional_area) * (1 + tempCoeff_Cu * (20 - 15)) #ohms
        total_current = voltage / coil_resistance * layers #Amps

total_power_consumption = math.pow(total_current, 2) * coil_resistance #Watts
total_power_consumption_mW = total_power_consumption * 1000 #milliwatts

# Coil Area Calculations
coil_area_per_layer_mm2 = 0

for i in range(0, rounded_loop_steps, 1):
    coil_area_per_layer_mm2 = 0.5 * math.pow((inner_radius + (spiral_thickness * (i / spiral_segments_per_turn))), 2) * ((2 * math.pi) / spiral_segments_per_turn) + coil_area_per_layer_mm2 #square millimeters

coil_area_mm2 = layers * (coil_area_per_layer_mm2 + (0.5 * math.pow((inner_radius + (spiral_thickness * ((i + 1) / spiral_segments_per_turn))), 2) * ((2 * math.pi) * number_of_spiral_turns_fraction))) #square millimeters

coil_area_m2 = coil_area_mm2 / 1000000 #square meters

# Sliding Plate Properties
aluminum_6060_density = 2710 #grams per cubic centimeter
slidingPlate_length = 64 * math.pow(10, -3) #meters
slidingPlate_width = 58 * math.pow(10, -3) #meters
slidingPlate_thickness = 1.6 * math.pow(10, -3) #meters

slidingPlate_volume = slidingPlate_length * slidingPlate_width * slidingPlate_thickness #meters cubed
slidingPlate_mass = aluminum_6060_density * slidingPlate_volume #Kilograms
slidingPlate_mmoi_xy = (1 / 12) * slidingPlate_mass * (math.pow(slidingPlate_length, 2) + math.pow(slidingPlate_width, 2)) #Kilograms meters squared

# PocketQube Body Properties
pq_body_length = 50 * math.pow(10, -3) #meters
pq_body_width = 50 * math.pow(10, -3) #meters
pq_body_height = 50 * math.pow(10, -3) #meters

pq_body_mass = 0.250 - slidingPlate_mass #Kilograms
pq_body_mmoi_xy = (1 / 12) * pq_body_mass * (math.pow(pq_body_length, 2) + math.pow(pq_body_width, 2)) #Kilograms meters squared

# PocketQube Properties
pq_mmoi_xy = pq_body_mmoi_xy + slidingPlate_mmoi_xy + (pq_body_mass + slidingPlate_mass) * math.pow((pq_body_height / 2) + (slidingPlate_thickness / 2), 2) #Kilograms meters squared

# Performance Calculations
magnetic_moment = total_current * coil_area_m2 #Amp meters-squared
n = magnetic_moment / total_current
efficiency = magnetic_moment / total_power_consumption_mW

torque = magnetic_moment * Bdot * math.sin(math.pi/2) #Newton meters
angluar_acceleration = torque / pq_mmoi_xy #radians per second squared
time_to_stabalize_seconds = desired_angular_velocity / angluar_acceleration #seconds
time_to_stabalize_minutes = time_to_stabalize_seconds / 60 #minutes

milliwattHours = total_power_consumption_mW * time_to_stabalize_minutes / 60 #milliwatt-hours

print("\nMagnetorquer Calculations:")
print("Layers:\t\t", layers)
print("Inner Diameter:\t", inner_diameter, "mm")
print("Outer Diameter:\t", outer_diameter, "mm")
print("Number of Turns:", 4 * round(number_of_turns_per_layer, 1))
print("Trace Width:\t", trace_width, "mm")
print("Trace Spacing:\t", clearance, "mm")
print("Trace Length:\t", int(coil_length), "mm")
print("Trace Thickness:", round(trace_thickness * 1000, 1), "um")
print("Coil Resistance\t", round(coil_resistance, 1), "Ohm")
print("Coil Area:\t", round(coil_area_m2, 3), "[m^2]")
print("Driven Voltage:\t", voltage, "V")
print("Current Draw:\t", round(total_current * 1000, 1), "mA")
print("Pwr Consumption:", round(total_power_consumption_mW, 1), "mW")
print("Magnetic Moment:", round(magnetic_moment, 4), "[Am^2]")
print("Efficiency:\t", format(efficiency, "0.6f"), "[Am^2]/[mW]")
print("MMOI:\t\t", round(pq_mmoi_xy * math.pow(10,5), 2), "[Kg][m^2]")
print("Torque:\t\t", round(torque, 12), "Nm")
print("Angular Accel:\t", round(angluar_acceleration, 6), "[rad]/[s^2]")
print("Rise Time:\t", round(time_to_stabalize_minutes, 2), "min")
print("MilliWatt Hours:", round(milliwattHours, 4), "mWh\n")