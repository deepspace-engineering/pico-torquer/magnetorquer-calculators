# Magnetorquer-Calculators
Calculators for characterizing PCB-embedded magnetorquers, specifically circular Archimedean spirals in PocketQube solar panels

- [Magnetorquer Preformance Calculator](https://gitlab.com/deepspace-engineering/pico-torquer/magnetorquer-calculators/-/blob/main/magnetorquerPreformance.py) - Characterizing design constraints obtaining parameters such as resistance, power, magnetic moment, and efficiency
- [Magnetic Field Calculator](https://gitlab.com/deepspace-engineering/pico-torquer/magnetorquer-calculators/-/blob/main/magfield.py) - Calculate a magnetic field vector from any point in any orbit (Perfect dipole model, EMAG integration coming soon!)
- [Detumbling Simulator](https://gitlab.com/deepspace-engineering/pico-torquer/magnetorquer-calculators/-/blob/main/bdot.py) - Satellite detumbling using a B-Dot controller
- [Nadir Pointing Simulator](https://gitlab.com/deepspace-engineering/pico-torquer/magnetorquer-calculators/-/blob/main/nadirPointing.py) - Satellite control system to point a desired axis towards the earth using a PD controller
