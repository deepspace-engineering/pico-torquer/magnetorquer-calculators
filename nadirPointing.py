import os
import numpy as np
import math
import matplotlib.pyplot as plt
from magfield import *

os.system('cls' if os.name == 'nt' else 'clear')

# Define orbit parameters
R_earth = 6.378e6 # meters
M_earth = 5.972e24 # kilograms
G = 6.6743e-11  # Gravitational constant
mu_0 = 4e-7 * np.pi # Permeability of free space

# Orbital elements
a = (567000 + R_earth) # Semi-major axis
e = 0.0009827  # Eccentricity
i = 97.6477 * np.pi / 180    # Inclination
raan = 49.4617 * np.pi / 180 # Right ascension of the ascending node
aop = 221.9916 * np.pi / 180 # Argument of periapsis
M0 = 140.6666 * np.pi / 180  # Mean anomaly at epoch

# Define simulation parameters
orbits = 1
T = int(2 * math.pi * math.sqrt(math.pow((567000 + R_earth), 3) / (6.6743e-11 * 5.972e24))) # Period
sample_rate = 10 # Hertz
dt = 1 / sample_rate # Simulation time step
t_max = T * orbits # Total simulation time
sim_time = np.arange(0, t_max, dt)

# Define sensor noise models
acc_noise_std = 0.01 # Standard deviation of accelerometer noise
omega_noise_std = 0.001 # Standard deviation of gyroscope noise

# Define sensor biases
acc_bias = np.array([0.0, 0.0, 0.01])  # Accelerometer bias
omega_bias = np.array([0.0, 0.0, 0.001])  # Gyroscope bias

# Define PD controller gains
K_p = np.array([0.000001, 0.000001, 0.000001])  # Proportional gain matrix
K_d = np.array([0.00000001, 0.00000001, 0.00000001])  # Derivative gain matrix

# Define spacecraft properties
inertia_tensor = np.array([[1.0556*math.pow(10,-4), 9.7529*math.pow(10,-6), 1.1174*math.pow(10,-5)], [6.2781*math.pow(10,-7), 1.1919*math.pow(10,-4), 1.1174*math.pow(10,-5)], [6.2781*math.pow(10,-7), 9.7529*math.pow(10,-6), 1.1295*math.pow(10,-4)]])  # Kg*m^2
m_sat = 0.25 # kilograms
alignment_vec = np.array([0.0, -1, 0.0])
M_max = 0.0029

# Initial spacecraft conditions
omega_sen = np.array([0.01, 0.01, 0.01])
theta_sen = np.array([0.01, 0.01, 0.01])
T_d = [0.0000000001, 0.0000000001, 0.0000000001]

# Initialize variables
mag_history = np.zeros((len(sim_time), 3))
acc_history = np.zeros((len(sim_time), 3))
omega_history = np.zeros((len(sim_time), 3))
theta_history = np.zeros((len(sim_time), 3))
time_history = np.zeros((len(sim_time), 1))
T_d_history = np.zeros((len(sim_time), 3))
nadir_vec_history = np.zeros((len(sim_time), 3))
rotation_angles_history = np.zeros((len(sim_time),3))
mag = np.zeros(3)
G_vec = np.zeros(3)
e_theta_prev = 0

# Simulate sensor data
def magnetometer(mu_0, G, M_earth, r_sc, n):
    mag = get_planetary_magnetic_field(mu_0, G, M_earth, r_sc)

    # Maybe add sensor noise here...
    #mag_noise = np.random.normal(0, mag_noise_std.value, 3)
    #mag_data[:, i] = (mag + mag_noise + mag_bias).value

    mag_data = mag
    mag_history[n, :] = mag_data

    return mag_data, mag_history

def accelerometer(T, t, e, a, i, G, aop, raan, M_earth, n, theta_sen):
    theta = true_anomaly_from_time(T, t, e)
    r_sc, v_sc = update_state_from_true_anomaly(theta, a, e, i, aop, raan)
    G_vec = (G * M_earth) / math.pow(np.linalg.norm(r_sc), 3) * r_sc
    theta_unit = theta_sen / np.linalg.norm(theta_sen)
    G_vec_sat = theta_unit * G_vec

    # Maybe add sensor noise here...
    #acc_noise = np.random.normal(0, acc_noise_std.value, 3)
    #acc_data[:, i] = (G_vec + acc_noise + acc_bias).value

    acc_data = G_vec_sat
    acc_history[n, :] = acc_data

    return acc_data, r_sc, v_sc, acc_history

def gyroscope(inertia_tensor, torque, dt, n):
    I_inv = np.linalg.inv(inertia_tensor)
    alpha = np.dot(I_inv, torque)
    omega = alpha * dt

    # Maybe add sensor noise here...
    #omega_noise = np.random.normal(0, omega_noise_std.value, 3)
    #omega_data[:, i] = (omega + omega_noise + omega_bias).value

    omega_data = omega
    omega_history[n, :] = omega_data

    return omega_data, omega_history

# Simulate control loop
for n, t in enumerate(sim_time):
    print("T+ ", round(t, 2), "s")

    # Get sensor data
    g_vec, r_sc, v_sc, acc_history = accelerometer(T, t, e, a, i, G, aop, raan, M_earth, n, theta_sen) # accelerometer
    omega_sen, omega_history = gyroscope(inertia_tensor, T_d, dt, n) # gyroscope
    B_field_body, mag_history = magnetometer(mu_0, G, M_earth, r_sc, n) # magnetometer
    
    # Calculate nadir vector
    nadir_vec = g_vec / np.linalg.norm(g_vec)
    nadir_vec_history[n, :] = nadir_vec
    
    # Calculate the rotation vector and angle
    rotation_axis_to_nadir = np.cross(alignment_vec, nadir_vec)
    rotation_angles = 2 * np.arcsin(rotation_axis_to_nadir)
    #rotation_vec = rotation_axis_to_nadir / np.linalg.norm(rotation_axis_to_nadir)
    rotation_angles_history[n, :] = rotation_angles


    # Calculate orientation
    theta_sen = omega_sen * dt
    e_theta = rotation_angles - theta_sen
    theta_history[n, :] = theta_sen

    # Calculate angular velocity error
    e_omega = (e_theta - e_theta_prev) / dt

    # Calculate desired magnetic moment using PD control
    M_d = K_p * e_theta + K_d * e_omega

    # Check if magnetic moment is within range
    M_d = np.clip(M_d, -M_max, M_max)

    # Calculate torque
    T_d = np.cross(M_d, B_field_body)
    T_d_history[n, :] = T_d

    #Define previous orientation state
    e_theta_prev = e_theta
    
# Save data
#np.savetxt("nadirData.csv", theta_history, delimiter=",")

plt.figure(figsize = (12, 6))

# Plot results
plt.title("Spacecraft Nadir Pointing")
plt.xticks([])
plt.yticks([])
plt.subplot(4, 1, 1)
plt.xlabel("Time (s)")
plt.ylabel("Orientation (rad)")
label1 = ['x', 'y', 'z']
plt.plot(sim_time, theta_history[:, 0], label = 'x')
plt.legend()

plt.subplot(4, 1, 2)
plt.xlabel("Time (s)")
plt.ylabel("Orientation (rad)")
plt.plot(sim_time, theta_history[:, 1], label = 'y')
plt.legend()

plt.subplot(4, 1, 3)
plt.xlabel("Time (s)")
plt.ylabel("Orientation (rad)")
plt.plot(sim_time, theta_history[:, 2], label = 'z')
plt.legend()

plt.subplot(4, 1, 4)
plt.xlabel("Time (s)")
plt.ylabel("Magnetometer (T)")
plt.plot(sim_time, mag_history, label = label1)
plt.legend()

plt.show()