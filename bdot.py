import os
import numpy as np
from scipy.integrate import odeint
from scipy.optimize import newton
import matplotlib.pyplot as plt
import math
from magfield import *

os.system('cls' if os.name == 'nt' else 'clear')

# Define orbital parameters
mu_0 = 4e-7 * np.pi # Permeability of free space
G = 6.6743e-11 # Gravitational constant
M_earth = 5.972e24 # Mass of Earth in kg
R_earth = 6.378e6 # Radius of Earth in m

# Define orbital elements
a = 567e3 + R_earth  # meters
e = 0.0009827
i = 97.6477 * np.pi / 180  # radians
aop = 221.9916 * np.pi / 180.0  # radians
raan = 49.4617 * np.pi / 180  # radians
true_anomaly = 0.0 * np.pi / 180  # radians

# Define time step and duration
orbits = 15
T = int(2 * math.pi * math.sqrt(math.pow(a, 3) / (G * M_earth)))
delta_t = 10 # seconds
t_max = T * orbits # seconds
sim_time = np.arange(0, t_max + delta_t, delta_t)

# Define initial conditions
theta_i = np.array([1.5, 1.5, 1.5])  # initial theta (roll, pitch, yaw)
omega_i = np.array([0.1, -0.1, -4.18])  # initial angular velocity (omega_x, omega_y, omega_z)

# Set desired final theta and velocity
omega_d = np.array([0, 1, 0])

# Define spacecraft properties
inertia_tensor = np.array([[1.0556*math.pow(10,-4), 9.7529*math.pow(10,-6), 1.1174*math.pow(10,-5)], [6.2781*math.pow(10,-7), 1.1919*math.pow(10,-4), 1.1174*math.pow(10,-5)], [6.2781*math.pow(10,-7), 9.7529*math.pow(10,-6), 1.1295*math.pow(10,-4)]])  # Identity matrix as an example
M_max = 0.0029

# Initialize variables
B_vec = []
y0 = np.concatenate(set_spacecraft_state_from_orbital_elements(a,e,i,aop,raan,true_anomaly))

def bdot_controller(B, omega, M_max):
    B_dot = -np.cross(omega, B)

    # Compute magnetic moment for each axis
    M = np.zeros(3)
    for i in range(3):
        if (B_dot[i] > 1 * math.pow(10,-7)):
            M[i] = -M_max * (B_dot[i]) / (np.linalg.norm(B_dot[i]))
        else:
            M[i] = 0
    
    return M

def simulate_bdot_controller_with_inertia(theta_i, omega_i, t_max, time_step, inertia_tensor, omega_d, T, e, a, i, aop, raan, y0, mu_0, G, M_earth, B_vec, M_max):
    time = np.arange(0, t_max, time_step)
    T_d_history = np.zeros((len(time), 3))
    alpha_history = np.zeros((len(time), 3))
    theta_history = np.zeros((len(time), 3))
    omega_history = np.zeros((len(time), 3))

    theta = theta_i
    omega = omega_i

    # Run simulation
    for j, sim_time in enumerate(time):
        print("T+ ", round(sim_time,2), "s")

        # Compute mean, eccentric, and true anomalies
        true_anomaly_calc = true_anomaly_from_time(T, sim_time, e)

        # Update initial state based on true anomaly (dependent on your specific model)
        r_sc, v_sc = update_state_from_true_anomaly(true_anomaly_calc, a, e, i, aop, raan)

        # Solve Kepler's equations
        y = odeint(kepler_equations, y0, [sim_time])

        # Calculate relative position vector
        r = r_sc - np.array([0, 0, 0])

        # Calculate magnetic field components at each time step
        B_p = get_planetary_magnetic_field(mu_0, G, M_earth, r)
        B_vec.append(B_p)

        # Update initial state for the next iteration
        y0 = np.concatenate((r_sc, v_sc))

        # Calculate magnetic moment vector with either B-dot control or a PD controller
        M_d = bdot_controller(B_p, omega, M_max)

        # Calculate T_d
        T_d = np.cross(M_d, B_p)

        # Calculate angular properties
        alpha = np.linalg.solve(inertia_tensor, T_d)
        omega += alpha * time_step
        theta = omega * time_step

        # Check if the desired theta and velocity are achieved
        if np.all(np.abs(omega - omega_d) < 1e-6):
            break

        # Store results
        T_d_history[j, :] = T_d
        alpha_history[j, :] = alpha
        omega_history[j, :] = omega
        theta_history[j, :] = theta
        
    return time, T_d_history, alpha_history, theta_history, omega_history, B_vec

# Run simulation
time, T_d_history, alpha_history, theta_history, omega_history, B_vec = simulate_bdot_controller_with_inertia(theta_i, omega_i, t_max, delta_t, inertia_tensor, omega_d, T, e, a, i, aop, raan, y0, mu_0, G, M_earth, B_vec, M_max)

# Save data
#np.savetxt("magData2.csv", B_vec, delimiter=",")

# Plot results
plt.figure(figsize = (12, 6))

plt.subplot(3, 1, 1)
plt.plot(time, theta_history)
plt.title("Spacecraft theta Over Time")
plt.xlabel("Time (s)")
plt.ylabel("theta (rad)")

plt.subplot(3, 1, 2)
plt.plot(time, omega_history)
plt.title("Angular Velocity Over Time")
plt.xlabel("Time (s)")
plt.ylabel("Angular Velocity (rad/s)")

plt.subplot(3, 1, 3)
plt.plot(time, np.array(B_vec)[:, 0], label='mag_x')
plt.plot(time, np.array(B_vec)[:, 1], label='mag_y')
plt.plot(time, np.array(B_vec)[:, 2], label='mag_z')
plt.title("Magnetic Field Components")
plt.xlabel("Time (s)")
plt.ylabel("Magnetic Field Strength (T)")
plt.tight_layout()

plt.show()